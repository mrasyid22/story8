from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'login'

urlpatterns = [
    path('', views.redirecting, name='redirect'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
]
