from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from login.views import login_view, redirecting

class UnitTest(TestCase):
    #Test views & urls
    def test_login_url_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_exist_is_not_exist(self):
        response = Client().get('/Sign-up/')
        self.assertEqual(response.status_code, 404)

    def test_login_using_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_login_using_login_view_function(self):
        response = resolve('/login/')
        self.assertEqual(response.func, login_view)
