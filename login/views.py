from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required

def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request,user)
            return redirect("homepage:search")

    else:
        form= AuthenticationForm()
    return render(request, "login.html", {'form':form})        
    
def logout_view(request):
    if request.method == "POST":
        logout(request)
        return redirect("login:login")
        
def redirecting(request):
    return redirect('/login/')