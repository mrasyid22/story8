from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from homepage.views import search, redirecting

class UnitTest(TestCase):
    #Test views & urls
    def test_search_url_exist(self):
        response = Client().get('/search-book/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_exist_is_not_exist(self):
        response = Client().get('/add-book/')
        self.assertEqual(response.status_code, 404)

    def test_search_book_using_search_template(self):
        response = Client().get('/search-book/')
        self.assertTemplateUsed(response, 'search.html')
    
    def test_search_book_using_search_function(self):
        response = resolve('/search-book/')
        self.assertEqual(response.func, search)
