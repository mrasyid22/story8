from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth.decorators import login_required

def search(request):
    if request.method == 'GET':
        return render(request, 'search.html')
        
def redirecting(request):
    return redirect('/search-book/')
